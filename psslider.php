<?php
/*
Plugin Name: psslider
*/
function psslider_setup_post_types() {

	$responsiveslider_labels =  apply_filters( 'ps_slider_labels', array(
		'name'                => 'PS header image slider',
		'singular_name'       => 'PS header image slider',
		'add_new'             => __('Add New', 'ps-header-image-slider'),
		'add_new_item'        => __('Add New Image', 'ps-header-image-slider'),
		'edit_item'           => __('Edit Image', 'ps-header-image-slider'),
		'new_item'            => __('New Image', 'ps-header-image-slider'),
		'all_items'           => __('All Image', 'ps-header-image-slider'),
		'view_item'           => __('View Image', 'ps-header-image-slider'),
		'search_items'        => __('Search Image', 'ps-header-image-slider'),
		'not_found'           => __('No Image found', 'ps-header-image-slider'),
		'not_found_in_trash'  => __('No Image found in Trash', 'ps-header-image-slider'),
		'parent_item_colon'   => '',
		'menu_name'           => __('PS Image Slider'),
		'exclude_from_search' => true
	) );


	$responsiveslider_args = array(
		'labels' 			=> $responsiveslider_labels,
		'public' 			=> true,
		'publicly_queryable'=> true,
		'show_ui' 			=> true,
		'show_in_menu' 		=> true,
		'query_var' 		=> true,
		'capability_type' 	=> 'post',
		'has_archive' 		=> true,
		'hierarchical' 		=> false,
		'menu_icon'   		=> 'dashicons-format-gallery',
		'supports' 			=> array('title','editor','thumbnail')
		
	);
	register_post_type( 'ps_slider', apply_filters( 'ps_post_type_args', $responsiveslider_args ) );

}
add_action('init', 'psslider_setup_post_types');

add_action( 'admin_menu', 'add_plugin_page' );
add_action( 'admin_init', 'page_init' );

function add_plugin_page()
{
	add_submenu_page('edit.php?post_type=ps_slider', 'Settings',  'Settings', 'manage_options', 'psslider-settings', 'create_admin_page');
}
function create_admin_page()
{ ?>
	<form method="post" action="options.php">
		<?php
				settings_fields( 'psslider_settings' );   
				do_settings_sections( 'psslider-settings' );
				submit_button(); 
		?>
		</form>

<?php
}
function page_init()
{
	    register_setting(
				'psslider_settings', // Option group
				'psslider_settings'
		);
		
        // Sections
		add_settings_section(
				'psslider_settings', // ID
				'Slider Settings', // Title
				'psslider_settings_behaviour_header', // Callback
				'psslider-settings' // Page
		);
        
		// Behaviour Fields
		add_settings_field(
				'interval', // ID
				'Slide Interval (milliseconds)', // Title
				'interval_callback', // Callback
				'psslider-settings', // Page
				'psslider_settings' // Section
		);
}

function psslider_settings_behaviour_header() {
            echo '<p>'.__('Basic setup of how each Carousel will function, what controls will show and which images will be displayed.', 'psslider-settings').'</p>';
	}
 function interval_callback() {
 	$options = get_option( 'psslider_settings' );
			printf('<input type="text" id="interval" name="psslider_settings[interval]" value="%s" size="15" />',
					isset( $options['interval'] ) ? esc_attr( $options['interval']) : '');
            echo '<p class="description">'.__('How long each image shows for before it slides. Set to 0 to disable animation.', 'psslider-settings').'</p>';
	}




